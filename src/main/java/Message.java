import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Message {
    enum CommandType {
        GET_NEXT_QUESTION,
        SEARCH_ANSWER,
        ANSWER
    }
    public Message (CommandType command){
        this.command = command;
    }

    private CommandType command;

    private String param;
}