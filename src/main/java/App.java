import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;


public class App {
    public static void main (String[] args){
        ActorSystem system = ActorSystem.create("test-system");
        ActorRef actorRef = system.actorOf(Props.create(MainActor.class), "main-actor");
        actorRef.tell(new Message(Message.CommandType.GET_NEXT_QUESTION), ActorRef.noSender());
    }
}
