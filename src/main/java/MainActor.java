import akka.Done;
import akka.actor.UntypedAbstractActor;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.*;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.util.ByteString;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import scala.concurrent.duration.FiniteDuration;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

public class MainActor extends UntypedAbstractActor {
    private final Http http = Http.get(context().system());
    private final Materializer materializer = ActorMaterializer.create(context());
    private final Scanner scanner = new Scanner(System.in);

    protected enum State {
        WAITING, SEARCHING, ANSWERING;
    }
    private State state = State.WAITING;

    @Override
    public void onReceive(Object message){
        switch (state){
            case WAITING:
                if (((Message)message).getCommand()== Message.CommandType.GET_NEXT_QUESTION)
                    receiveQuestion();
                break;
            case SEARCHING:
                if (((Message)message).getCommand()== Message.CommandType.SEARCH_ANSWER)
                    searchQuestion(((Message)message).getParam());
                break;
            case ANSWERING:
                if (((Message)message).getCommand()== Message.CommandType.ANSWER)
                    answer(((Message)message).getParam());
                break;
        }
    }

    private void receiveQuestion (){

        System.out.println("Please, ask your question");
        String question = scanner.nextLine();
        state = State.SEARCHING;
        getSelf().tell(new Message(Message.CommandType.SEARCH_ANSWER, question), getSelf());
    }

    private void searchQuestion(String question){
        System.out.println("Searching...");
        HttpRequest request = HttpRequest.POST("https://odqa.demos.ivoice.online/model")
                .withEntity(ContentTypes.APPLICATION_JSON, ByteString.fromString("{\"context\": [ \"" + question + "\" ]}"))
                .addHeader(HttpHeader.parse("accept","application/json"));
        CompletionStage<Done> completion =
                Source.single(request) // : HttpRequest
                        .mapAsync(1, http::singleRequest) // : HttpResponse
                        .runWith(Sink.foreach( this::parseResponse), materializer);

        if (state==State.SEARCHING) {
            state = State.ANSWERING;
            getSelf().tell(new Message(Message.CommandType.ANSWER), getSelf());
        }
    }

    private void parseResponse (HttpResponse response){
        String answer = null;
        if (response.status().intValue()==200){
            final CompletionStage<HttpEntity.Strict> strictEntity = response.entity()
                    .toStrict(FiniteDuration.create(3, TimeUnit.SECONDS).toMillis(), materializer);

            final CompletionStage<String> stringStage = strictEntity
                    .thenCompose(strict ->
                            strict.getDataBytes()
                                    .runFold(ByteString.empty(), ByteString::concat, materializer)
                                    .thenApply(ByteString::utf8String)
                    );
            try {
                String json = stringStage.toCompletableFuture().get();
                ObjectMapper mapper = new ObjectMapper();
                List<List<List<String>>> obj = mapper.readValue(json, new TypeReference<List<List<List<String>>>>() {
                    @Override
                    public Type getType() {
                        return super.getType();
                    }
                });
                answer = obj.get(0).get(0).get(0);
            }
            catch (Exception e){
                e.printStackTrace();
            };
        }
        state = State.ANSWERING;
        getSelf().tell(new Message(Message.CommandType.ANSWER, answer), getSelf());

    }

    private void answer (String answer) {
        if (answer == null)
            System.out.println("Error getting answer");
        else
            System.out.println(answer);
        System.out.println("Ask next question? Y/n");
        while (true) {
            String input = scanner.nextLine();
            if (input.toLowerCase().equals("y") || input.toLowerCase().equals("yes")) {
                state = State.WAITING;
                getSelf().tell(new Message(Message.CommandType.GET_NEXT_QUESTION), getSelf());
                break;
            } else if (input.toLowerCase().trim().equals("n") || input.toLowerCase().trim().equals("no")) {
                Runtime.getRuntime().exit(0);
            }
            System.out.println("Ask next question? Y/n");
        }
    }


}
